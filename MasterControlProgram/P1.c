#include "p1fxns.h"
#include "queue.h"
#include "process.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <signal.h>

#define BUFFERSIZE 256

volatile int USR1_received = 0;
volatile int processAlive  = 0;

Process *current_process;
int *pid_list;

Process *createProcess(int numArgs);
void destroyProcess();

Queue *process_queue;

/*This portion is the functions for queue */

void init_queue()
{
    process_queue = malloc(sizeof(Queue));
    if (process_queue == NULL){
        p1perror(2, "Error: init_queue() failed to malloc");
        exit(1);
    }
    process_queue->initialSize = 0;
    process_queue->head        = NULL;
    process_queue->tail        = NULL;

}

void enqueue(Process *process)
{
    ProcessNode *new = malloc(sizeof(ProcessNode));
    if (new == NULL)
    {
        p1perror(2, "Error: enqueue() failed to malloc");
        exit(1);
    }
    new->process = process;
    new->next = NULL;
    if(process_queue->head == NULL)
    {
        process_queue->head = new;
        process_queue->tail = new;
    }else
    {
        process_queue->tail->next = new;
        process_queue->tail = new;
    }
}

Process *dequeue()
{
    if (process_queue->head == NULL)
    {
        p1perror(2,"Error: dequeue() - queue is empty");
        exit(1);
    }
    Process *head = process_queue->head->process;
    ProcessNode *temp = process_queue->head;
    process_queue->head = process_queue->head->next;
    temp->next = NULL;
    free(temp);
    return head;
}

unsigned int isEmpty()
{
    if (process_queue->head == NULL)
    {
        return 1;
    }
    else
    {
        return 0;
    }

}

void deleteQueue()
{
    while(!isEmpty())
    {
        Process *tmp = dequeue();
        destroyProcess(tmp);//check this
    }
    free(process_queue);
    process_queue = NULL;
}

/* functions for processes */

Process *createProcess(int numArgs)
{
    Process *p_struct = (Process *)malloc(sizeof(Process));

    if (p_struct->args != NULL)
    {
        p_struct->args = (char **)malloc((numArgs+1) * sizeof(char*));

        if(p_struct->args == NULL)
        {
            free(p_struct);
            return p_struct = NULL;
        }
        p_struct->numArgs = numArgs;
        p_struct->cmd = NULL;
        p_struct->pid = -1;
        p_struct->status = 2;
    }
    return p_struct;
}

void destroyProcess(Process *process)
{
    free(process->cmd);

    int i = 0;
    char *arg;
    while (i<= process->numArgs)
    {
        arg = process->args[i+1];
        free(arg);
    }
    free(process->args);
    free(process);
}

/*-------------------------------------------------*/

void strip_new_line(char word[])
{
    int i = 0;
    while (word[i] != '\0')
    {
        if(word[i] == '\n')
        {
            word[i] = '\0';
            break;
        }
        i++;

    }
}

void setQueue(int fd)
{
    Process *current = NULL;
    char buffer[BUFFERSIZE];
    int n;

    while((n = p1getline(fd, buffer, sizeof(buffer))) > 0)
    {
        int numArgs = 0;
        char word_buffer[100];
        char temp[BUFFERSIZE];
        int i = 0;

        p1strcpy(temp,buffer);
        while((i = p1getword(temp, i, word_buffer)) > 0)
        {
            numArgs++;
        }
        current = createProcess(numArgs);
        if(current == NULL)
        {
            exit(1);
        }
        char word[100];
        int j = 0;

        p1getword(buffer,0,word);
        strip_new_line(word);
        current->cmd = p1strdup(word);

        int index = 0;
        while ((j = p1getword(buffer, j, word)) > 0)
        {
            strip_new_line(word);
            current->args[index++] = p1strdup(word);
        }
        current->args[index] = NULL;
        process_queue->initialSize++;
        enqueue(current_process);
    }
}

void program(int argc, char *argv[])
{
    char *file_name = NULL;
    int fd;

    if (argc > 1)
    {
        if (p1strneq(argv[1], "-", 1))
        {
            file_name = argv[2];
        }
        else if (p1strneq(argv[2], "-", 1))
        {
            file_name = argv[2];
        }
    }
    if (file_name == NULL)
    {
        fd = 0;
        setQueue(fd);
    }
    else
    {
        fd = open(file_name, 0);
        setQueue(fd);
    }


}

void fork_programs()
{
    int *pidlist;
    int programs = process_queue->initialSize;

    pidlist = (int *)malloc(programs * sizeof(int));
    if(pidlist == NULL)
    {
        exit(1);
    }
    ProcessNode *current = process_queue->head;
    int i = 0;
    while(current != NULL){
        pidlist[i] = fork();
        if(pidlist[i] == 0){
            char *cmd = current->process->cmd;
            char **args = current->process->args;
            execvp(cmd,args);

            p1perror(2, "execvp fail");
            exit(1);
        }
        current = current->next;
        i++;
    }
    int j;
    for(j = 0; j < programs; j++)
    {
        waitpid(pidlist[i],0,0);
    }

    free(pidlist);
}

int main(int argc, char *argv[])
{
    init_queue();
    program(argc,argv);

    if(isEmpty())
    {
        p1perror(2, "empty queue");
        exit(1);
    }
    fork_programs();
    deleteQueue();
    exit(0);
}




















