#ifndef QUEUE_H
#define QUEUE_H
#include <stdlib.h>
#include "process.h"

typedef struct ProcessNode 
{
    Process *process;
    struct ProcessNode *next;
}ProcessNode;

typedef struct
{
    int initialSize;
    ProcessNode *head;
    ProcessNode *tail;
} Queue;
#endif
