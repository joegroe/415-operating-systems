#ifndef PROCESS_H
#define PROCESS_H

typedef struct Process {
    char *cmd;
    char **args;
    int pid;
    int status;
    int numArgs;
}Process;
#endif
